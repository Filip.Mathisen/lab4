package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private CellState initialState;
    private Integer rows;
    private Integer columns;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.grid = new CellState[rows][columns];

        for (int i=0; i < rows; i++) {
            for (int y = 0; y < columns; y++) {
                grid[i][y] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        this.grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copy = new CellGrid(rows, columns, initialState);
        
        for (int i = 0; i < this.rows; i++) {
            for (int y = 0; y < this.columns; y++) {
                copy.grid[i][y] = this.grid[i][y];
            }
        }
        return copy;
    }
}
